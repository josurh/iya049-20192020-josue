function Cons(head, tail) {
        this.head = head;
        this.tail = tail;
    }
    
    Cons.prototype.map = function (fn) {
        const array = []
        array.push(fn(this.head))
        array.push(fn(this.tail))
        return array;
    }
    
    Cons.prototype.getType = () => {
        return "List";
    }
    
    function Nil() {
    
        this.nil=nil;
    
        Nil.prototype.getType = () => {
            return "List";
        }
        return nil;
    }
    module.exports = {Cons, Nil};