const Cons = (head, tail) => {
    const cons = {};
        cons.head = head;
        cons.tail = tail;
        cons.getType = () => {    
            return "List";

        }  
        cons.map = (fn) => { 
            const array = []
            array.push(fn(this.head))
            array.push(fn(this.tail))
            return array
        }  
        return cons;
    }; 
    const Nil = () => {
        this.nil=nil;
        nil.getType = () => {
            return "List";
        };
        return nil;  
    };
    module.exports = {Cons, Nil};